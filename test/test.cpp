#include "../src/linked_list.hpp"
#include <iostream>
#include <string>

template<typename T>
void print(const ll::linked_list<T> &list, const std::string &s = "") {
	// A print of the count before showing the elements
	std::clog << (s != "" ? (s + '\n') : s) << "Count: " << list.get_node_count() << '\n';

	/* The lame way to access the iterator
	   ll::node_iter::iter<int> it = list.begin();
	   ll::node<int> *temp = *it;
	 */

	// The nice way with dereferrence operator
	for (ll::node_iter::iter<int> it = list.begin(); *it != nullptr; ++it) {
		std::clog << *it << '\n';
	}

	std::cout << '\n';
}

int main() {
	ll::linked_list<int> list;

	list.push_back(23);
	list.push_back(101);
	list.push_back(48);
	list.push_back(2999);

	ll::node_iter::iter<int> it = list.begin();
	// Should be on 101 now
	++it;

	print(list);

	list.kill(it);

	print(list, "Killing iterator @ 101...");
	// This should be on 23 now
	it = list.begin();
	// Deref iter, then deref the ptr again to get the actual node
	// This should give us 23+=411=434
	**it += 411;

	print(list, "Doing head (23) += 411, which is 434...");

	list.kill(list.get_tail());

	print(list, "Killing tail...");

	list.kill(list.get_head());

	print(list, "Killing head...");

	return 0;
}
