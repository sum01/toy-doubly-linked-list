# Info

A toy project to implement a doubly-linked list. This includes its own custom iterator.

Everything is templated so any data can be held in the nodes. Many things untested.

## Dependencies

[Cmake](https://cmake.org/) v3.8 (or newer) is needed for building.

A C++11 compatible compiler is also needed.

## Build & Install

Note that if you don't want to install, remove `--target install` from the last command.  
If you do that, you can also run as non-privledged (i.e. no `sudo` or `runas`)

Linux/macOS:

```
mkdir -p build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTING=OFF ..
sudo cmake --build . --target install
```

Windows:

```
mkdir build
cd build
cmake -DBUILD_TESTING=OFF ..
runas /user:Administrator "cmake --build . --config Release --target install"
```
