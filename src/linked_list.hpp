#ifndef TOY_LINKED_LIST_HPP
#define TOY_LINKED_LIST_HPP

#include <stdexcept>
#include <ostream>

namespace ll {
	template<typename T>
	class node {
private:
		// The {} tells it to use default initialization for whatever type it is
		// Ref: https://stackoverflow.com/a/2143038
		T data = {};
		ll::node<T> *prev = nullptr, *next = nullptr;
public:
		// Construct using the default (empty) values
		explicit node() noexcept {
		}
		explicit node(const T &input_data) noexcept
			: data(input_data) {
		}
		T get() const noexcept {
			return this->data;
		}
		ll::node<T> * get_next() const noexcept {
			return this->next;
		}
		ll::node<T> * get_prev() const noexcept {
			return this->prev;
		}
		void set_next(ll::node<T> *n) noexcept {
			this->next = n;
		}
		void set_prev(ll::node<T> *p) noexcept {
			this->prev = p;
		}
		T operator++() noexcept {
			return ++this->data;
		}
		T operator++(int) noexcept {
			const T temp = this->data;
			++this->data;
			return temp;
		}
		T operator--() noexcept {
			return --this->data;
		}
		T operator--(int) noexcept {
			const T temp = this->data;
			--this->data;
			return temp;
		}
		T operator*(const ll::node<T> *rhs) {
			return this->data * rhs->get();
		}
		T operator*=(const ll::node<T> *rhs) {
			return this->data *= rhs->get();
		}
		T operator/(const ll::node<T> *rhs) {
			return this->data / rhs->get();
		}
		T operator/=(const ll::node<T> *rhs) {
			return this->data /= rhs->get();
		}
		T operator+(const ll::node<T> *rhs) {
			return this->data + rhs->get();
		}
		T operator+=(const ll::node<T> *rhs) {
			return this->data += rhs->get();
		}
		T operator-(const ll::node<T> *rhs) {
			return this->data - rhs->get();
		}
		T operator-=(const ll::node<T> *rhs) {
			return this->data -= rhs->get();
		}
		T operator*(const T &rhs) {
			return this->data * rhs;
		}
		T operator*=(const T &rhs) {
			return this->data *= rhs;
		}
		T operator/(const T &rhs) {
			return this->data / rhs;
		}
		T operator/=(const T &rhs) {
			return this->data /= rhs;
		}
		T operator+(const T &rhs) {
			return this->data + rhs;
		}
		T operator+=(const T &rhs) {
			return this->data += rhs;
		}
		T operator-(const T &rhs) {
			return this->data - rhs;
		}
		T operator-=(const T &rhs) {
			return this->data -= rhs;
		}
		// Easy printing friend function so a dereferrenced iterator works to print
		friend std::ostream & operator<<(std::ostream &out, ll::node<T> *n) {
			return out << n->get();
		}
	};

	namespace node_iter {
		template<typename T>
		class iter {
private:
			ll::node<T> * data = nullptr;
public:
			explicit iter(ll::node<T> *d) noexcept
				: data(d) {
			}
			node<T> * operator++() noexcept {
				if (this->data != nullptr) {
					this->data = this->data->get_next();
				}
				return this->data;
			}
			node<T> * operator--() noexcept {
				if (this->data != nullptr) {
					this->data = this->data->get_prev();
				}
				return this->data;
			}
			// A derefference operator overload
			// So *iter will return the pointer this iter contains
			node<T> * operator*() const noexcept {
				return this->data;
			}
		};
	}

	template<typename T>
	class linked_list {
private:
		unsigned int size = 0;
		ll::node<T> *head = nullptr, *tail = nullptr;
public:
		explicit linked_list() noexcept {
		}
		// Returns the first node constructed into an iter
		ll::node_iter::iter<T> begin() const noexcept {
			return ll::node_iter::iter<T>(this->head);
		}
		// Not how std iters use nullptr as an end, but the actual end
		ll::node_iter::iter<T> end() const noexcept {
			return ll::node_iter::iter<T>(this->tail);
		}
		// Returns the head node
		node<T> * get_head() const noexcept {
			return this->head;
		}
		// Returns the last node or nullptr
		node<T> * get_tail() const noexcept {
			return this->tail;
		}
		// Since the last node always has an empty ->next, this is safe
		void push_back(const T &d) noexcept {
			ll::node<T> *temp = new ll::node<T>(d);
			// Get the last node, set its next value to a new node
			if (this->head == nullptr) {
				this->head = temp;
				this->tail = this->head;
			} else {
				this->tail->set_next(temp);
				temp->set_prev(this->tail);
				this->tail = temp;
			}
			++this->size;
		}
		const unsigned int & get_node_count() const noexcept {
			return this->size;
		}
		void push_front(const T &d) noexcept {
			ll::node<T> *temp = new ll::node<T>(d);
			// Since nodes is the first element, this is fine
			temp->set_next(this->head);
			this->head->set_prev(temp);
			this->head = temp;
			++this->size;
		}
		// Push a new node between the current node and whatever is before it
		void push_before(const ll::node_iter::iter<T> &it, const T &in_data) {
			ll::node<T> *current_node = *it;
			if (current_node == nullptr) {
				throw std::invalid_argument("That iterator is null");
			}
			ll::node<T> *temp = new ll::node<T>(in_data);
			// If it's already set, look in both directions and empty out both
			temp->set_next(current_node);
			// If the current nodes prev is null, it's the front node
			if (current_node->get_prev() == nullptr) {
				// Since we're pushing a new front node, set the new node as the head
				this->head = temp;
			} else {
				// Set temp's prev to our current nodes prev
				temp->set_prev(current_node->get_prev());
				// Now set the previous nodes next to the node we pushed between it
				current_node->get_prev()->set_next(temp);
			}
			current_node->set_prev(temp);
			++this->size;
		}
		void push_after(const ll::node_iter::iter<T> &it, const T &in_data) {
			ll::node<T> *current_node = *it;
			if (current_node == nullptr) {
				throw std::invalid_argument("That iterator is null");
			}
			ll::node<T> *temp = new ll::node<T>(in_data);
			// Set temp's prev to our current nodes prev
			temp->set_prev(current_node);
			// If the current nodes prev is null, it's the front node
			if (current_node->get_next() != nullptr) {
				temp->set_next(current_node->get_next());
				// Now set the previous nodes next to the node we pushed between it
				current_node->get_next()->set_prev(temp);
			}
			current_node->set_next(temp);
			++this->size;
		}
		void kill(const ll::node<T> *current_node) {
			// Don't try to kill on an empty linked list
			if (this->size == 0) {
				throw std::logic_error("The linked list is already empty");
			}
			if (current_node == nullptr) {
				throw std::invalid_argument("That iterator is null");
			}
			ll::node<T> *temp_prev = current_node->get_prev(), *temp_next = current_node->get_next();
			if (temp_next != nullptr) {
				temp_next->set_prev(temp_prev);
			} else {
				// There's no next if we get in here, so its the tail
				// Since we're deleting it, set the tail to whatever the nodes->prev is
				// This also handles setting it to nullptr since get_prev() would be nullptr (if empty)
				this->tail = temp_prev;
			}
			// Link the previous & next to each other, since we're removing a node
			if (temp_prev != nullptr) {
				temp_prev->set_next(temp_next);
			} else {
				// There's no previous if we get in here, so its the head
				// Since we're deleting it, set the head to whatever the nodes->next is
				// This also handles setting it to nullptr since get_next() would be nullptr (if empty)
				this->head = temp_next;
			}
			--this->size;
			delete current_node;
		}
		void kill(const ll::node_iter::iter<T> &it) {
			ll::node<T> * current_node = *it;
			this->kill(current_node);
		}
	};
}

#endif
